## 1. 配置向导

在上一篇文章 [01.OPNsense安装](./01.OPNsense安装.md) 中，已经安装好了 OPNsense 。  

由于 OPNsense 系统更新较快，尤其是安全更新，平均 `2` 周左右就会收到一次小更新。  

因此，多数情况下使用 OPNsense ISO 镜像安装的系统并非为最新版本。  

**建议在 OPNsense 安装完成后，优先对系统进行一次跟新，然后再恢复一次出厂设置。**  

### 1.1 登录

默认情况下，OPNsense 的 IPv4 地址为 `192.168.1.1` 。  

将电脑网络设置为与 `192.168.1.0/24` 同网段地址，例如 `192.168.1.100/24` 。  

用网线将电脑网口与 OPNsense 最后一个网口连接，并访问 `https://192.168.1.1` 。  

OPNsense 默认账户为 `root` ，初始化密码为 `opnsense` ，使用该密码进行登录。  

![登录OPNsense](img/p02/opn_login.jpeg)

稍等片刻，系统将自动跳转到配置向导，点击 `Next` 。  

![wizard开始](img/p02/opn_wizard_next.jpeg)

### 1.1 General Information

`General Information` 中有以下内容需要调整：  

| 参数                  | 值                     | 说明                               |
| --------------------- | ---------------------- | ---------------------------------- |
| Hostname              | `opnsense`             | 系统主机名，建议用小写英文字母填写 |
| Domain                | `xxxxxxx`              | 系统本地域名                       |
| Language              | `Chinese (Simplified)` | 系统 WEB 界面语言，修改为简体中文  |
| Primary DNS Server    | `223.5.5.5`            | 阿里 DNS，支持 IPv4 IPv6 DNS 地址  |
| Secondary  DNS Server | `119.29.29.29`         | 腾讯 DNS，支持 IPv4 IPv6 DNS 地址  |
| Override DNS          | **取消勾选**           | 是否允许系统 DNS 被上游协议覆盖    |

> 界面样式：
>
> ![wizard一般信息](img/p02/opn_wizard_gi.jpeg)

### 1.3 Time Server Information

调整内容：

| 项目                 | 值                                                           | 说明 |
| -------------------- | ------------------------------------------------------------ | ---- |
| Time server hostname | `0.time.pool.aliyun.com 1.cn.ntp.org.cn 2.ntp.ntsc.ac.cn 3.time1.cloud.tencent.com` | 替换 |
| Timezone             | `Asia/Shanghai`                                              |      |

按需修改完成后，点击 `Next` 。

> 界面样式：
>
> ![wizard时区](img/p02/opn_wizard_tz.jpeg)

### 1.4 配置 WAN 接口

#### 1.4.1 PPPoe 拨号主路由

本文将使用 OPNsense `PPPoE` 拨号并配置 IPv6 ，因此 `WAN` 接口设置如下。

| 参数                  | 值       | 说明                             |
| --------------------- | -------- | -------------------------------- |
| IPv4 配置类型         | `PPPoE`  | IPv4 联网方式，选择 `PPPoE`      |
| PPPoE 用户名          | 拨号账户 | `PPPoE` 拨号账户                 |
| PPPoE 密码            | 拨号密码 | `PPPoE` 拨号密码                 |
| 阻止 RFC1918 私有网络 | **勾选** | 即使是非公网 IPv4 地址，也需勾选 |
| 拦截 bogon 网络       | **勾选** | 即使是非公网 IPv4 地址，也需勾选 |

按需修改完成后，点击 `下一步` 。  

![wizardWAN](img/p02/opn_wizard_wan_dhcp.jpeg)

#### 1.4.2  普通路由

若不作 PPPoe 拨号主路由，`WAN` 配置步骤，保持 `IPv4配置类型` 为 `DHCP` 不变。  

**取消勾选** 页面底部的 `阻止RFC1918私有网络` 和 `拦截bogon网络` 选项，并点击 `下一步` 。  

![wizardWAN](img/p02/opn_wizard_wan_dhcp.jpeg)

如果模式下 `LAN` 地址（默认 `192.168.1.1`）与 `前置路由器` 或 `光猫` IPv4 地址冲突，需要手动调整。  

手动调整 `LAN IP地址` 后，需要同步手动调整电脑网卡的静态 IPv4 地址。  

### 1.5 配置 LAN 接口

`LAN` 配置步骤需要注意，OPNsense 默认 `LAN IP地址` 为 `192.168.1.1` 。  

本文将使用 `192.168.1.0/24` 作为内网网段，因此 `LAN` 接口设置如下。 

| 参数        | 值            | 说明                 |
| ----------- | ------------- | -------------------- |
| LAN IP 地址 | `192.168.1.1` | 系统 `LAN` IPv4 地址 |
| 子网掩码    | `24`          | 即 `255.255.255.0`   |

![wizardLAN](img/p02/opn_wizard_lan.jpeg)

### 1.6 设置 Root 密码

修改密码后，点击 `下一步` 。  

![wizard密码](img/p02/opn_wizard_pwd.jpeg)

### 1.6 重新加载配置

系统需要重新加载配置，点击 `重新加载` 。  

![wizard重载](img/p02/opn_wizard_reload.jpeg)

### 1.7 进一步配置 WAN 接口

打开左侧导航 `接口 - WAN` 页面，更新 `WAN` 接口设置如下。  

| 配置分组          | 参数             | 值       |
| ----------------- | ---------------- | -------- |
| 基本配置          | 启用             | 勾选     |
|                   | 锁定             | 勾选     |
| 通用配置          | 阻止私有网络     | **勾选** |
|                   | 拦截 bogon 网络  | **勾选** |
|                   | IPv4 配置类型    | `PPPoE`  |
|                   | IPv6 配置类型    | `DHCPv6` |
|                   | MTU              |          |
| PPPoE 配置        | 用户名           | 拨号账户 |
|                   | 密码             | 拨号密码 |
| DHCPv6 客户端配置 | 配置模式         | `基本`   |
|                   | 仅请求 IPv6 前缀 | **勾选** |
|                   | 发送IPv6前缀提示 | **勾选** |
|                   | 前缀委派大小     | `60`     |
|                   | 使用 IPv4 连接   | **勾选** |

**额外说明：**  

1. `MTU` 参数默认值为 `1492` ，该参数与运营商相关，多数情况下无需修改（留空）。  

2. `前缀委派大小` 参数默认值为 `64` ，该参数与运营商相关，建议与运营商提供的参数保持一致。  

   2.1. 国内环境，该参数一般为 `56` 、 `60` 、 `64` 。  

   2.2. 不同的 `前缀委派大小` 将影响内网 IPv6 子网数量。  

   2.3. 国内运营商 `前缀委派大小` 参考如下，欢迎提交信息。  

   - 电信宽带： `56`  
   - 移动宽带： `60`  
   - 联通宽带： `60`  

按需修改完成后，点击页面底部 `保存` 并 `应用更改` 。 

![WAN接口设置](img/p02/opn_nic_modify_wan.jpeg)

## 2. 更新系统

打开左侧导航 `系统 - 固件 - 设置` 页面。  

`镜像` 设置为 `(other)` ，此时系统会显示额外输入框，允许用户自定义系统镜像。  

在对话框中输入 OPNsense 镜像站地址，这里以 `北京大学镜像站` 地址为例，并点击 `保存` 。  

**注意：镜像地址链接结尾处，不要有多余的符号 `/` 。**  

```bash
## 北京大学镜像站
https://mirrors.pku.edu.cn/opnsense

## 北京外国语大学镜像站
https://mirrors.bfsu.edu.cn/opnsense

## 网易镜像站
https://mirrors.163.com/opnsense
```

![OPNsense镜像设置](img/p02/opn_mirror.jpeg)

点击 `状态` 选项卡，可以看到当前系统镜像已被设置为北京大学镜像站地址，点击 `检查升级` 。  

![OPNsense检查升级](img/p02/opn_check_update.jpeg)

系统会自动跳转到 `更新` 选项卡，并从镜像站同步数据。  

当系统检测到更新时，会弹出近期更新的 `Changelog` ，并显示待更新软件包列表。  

点击页面底部的 `更新` ，系统在更新完成后自动重启。  

![OPNsense系统升级](img/p02/opn_upgrade.jpeg)

## 3.恢复出厂设置

**注意：**  

1. 恢复出厂设置后，OPNsense 不会自动重启，需要手动启动。  

2. 需要在系统引导过程中，重新指定 `WAN / LAN` 对应的网口名称。  

3. 系统启动完成后，将再次使用 `配置向导` 对系统进行初始化配置。  

使用默认账户及密码重新登录 OPNsense ，打开左侧导航 `系统 - 固件 - 状态` 页面，检查更新后状态。  

![OPNsense升级后确认](img/p02/opn_post_check.jpeg)

原作者：

> 出于 "移除 OPNsense 配置文件中‘过时’ 配置项" 等目的 **建议** 执行一次恢复出厂设置操作。

打开左侧导航 `系统 - 配置 - 默认` 页面，点击 `是` ，即可恢复出厂设置。  

![OPNsense恢复出厂设置](img/p02/opn_restore_defaults.jpeg)
